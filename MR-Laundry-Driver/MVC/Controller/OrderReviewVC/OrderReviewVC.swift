//
//  OrderReviewVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 11/11/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class OrderReviewVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.configInitialView()
        
        //tblView.tableHeaderView = headerView
    }
    
    
    func configInitialView() {
        
        Helper().addHeader(forViewController: self,
                           isleftBtnImg: true, leftBtnName: "menu",
                           isleftBtnImg2: false, leftBtnName2: "",
                           centerName:"ORDER REVIEW",
                           isRightBtnImg: true, rightBtnName: "menu",
                           isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        tblView.tableFooterView = UIView.init()
    }
    
    //MARK:- HeaderView Button Action
    func leftHeaderBtnAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
        self.performSegue(withIdentifier: "OrderStatusVC", sender: nil)
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    // MARK: - UITable view delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
            return 0
        }else if section == 2{
            return 1
        }else if section == 3{
            return 1
        }
        return 2 //Need Section wise
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let imagView = cell.viewWithTag(1) as! UIImageView
        imagView.image = #imageLiteral(resourceName: "delivery")
        imagView.contentMode = .scaleAspectFit
        
        let textLabel = cell.viewWithTag(2) as! UILabel
        textLabel.text = "Test App"
        
        if indexPath.section == 0 {
            if indexPath.row == 1{
            imagView.image = #imageLiteral(resourceName: "pick_up")
            }else{
                imagView.image = #imageLiteral(resourceName: "drop")
            }
        }else if indexPath.section == 1 {
            imagView.isHidden = true
        }else if indexPath.section == 2 {
                imagView.image = #imageLiteral(resourceName: "service")
        }else if indexPath.section == 3{
            imagView.image = #imageLiteral(resourceName: "location")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

            let header = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
            header.backgroundColor = UIColor.init(red: 112/255.0, green: 193/255.0, blue: 248/255.0, alpha: 1)
        
        //Label
        let name = UILabel.init(frame: CGRect(x: 10, y: 0, width: self.view.frame.width/2 - 5, height: 40))
        name.font = UIFont.boldSystemFont(ofSize: 15)
        name.text = "John Smith"
        //ID
        let orderId = UILabel.init(frame: CGRect(x: name.frame.origin.x + name.frame.size.width, y: 0, width: self.view.frame.width/2 - 10, height: 40))
        orderId.font = UIFont.boldSystemFont(ofSize: 15)
        orderId.textAlignment = .right
        orderId.text = "Order ID : 66988"
        
        if section == 1 {
            name.text = "Items"
            orderId.isHidden = true
        }else if section == 2{
           name.text = "Service"
            orderId.isHidden = true
        }else if section == 3{
            name.text = "Address"
            orderId.isHidden = true
        }else if section == 4{
            name.text = "Price"
            orderId.text = "$600"
        }
        
        header.addSubview(name)
        header.addSubview(orderId)
        
        return header
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
