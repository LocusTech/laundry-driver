//
//  SelectTimeVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 11/11/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
}

class SelectTimeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var items = ["8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM", "8 AM to 9 AM"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.configInitialView()
        
        
    }
    
    
    func configInitialView() {
        
        Helper().addHeader(forViewController: self,
                           isleftBtnImg: true, leftBtnName: "menu",
                           isleftBtnImg2: false, leftBtnName2: "",
                           centerName:"TIME",
                           isRightBtnImg: true, rightBtnName: "back_arrow_white",
                           isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        
        
    }
    
    //MARK:- HeaderView Button Action
    func leftHeaderBtnAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
        self.performSegue(withIdentifier: "UserVC", sender: nil)
    }
    
    func rightHeaderBtnAction2() {
        
    }

    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        //cell.myLabel.text = self.items[indexPath.item]
        let label = cell.viewWithTag(1) as! UILabel
        label.text = self.items[indexPath.row]
        cell.backgroundColor = UIColor.white // make cell more visible in our example project
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 10, height: 10)
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
