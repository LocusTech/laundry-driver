//
//  OrderStatusVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 11/11/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class OrderStatusVC: UIViewController {

    
    @IBOutlet weak var pickedUpView: UIView!
    @IBOutlet weak var atLaundryView: UIView!
    @IBOutlet weak var deliveredView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.configInitialView()
    }
    
    
    func configInitialView() {
        
        Helper().addHeader(forViewController: self,
                           isleftBtnImg: true, leftBtnName: "menu",
                           isleftBtnImg2: false, leftBtnName2: "",
                           centerName:"ORDER STATUS",
                           isRightBtnImg: false, rightBtnName: "",
                           isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
        
        pickedUpView.layer.cornerRadius = 6.0
        pickedUpView.clipsToBounds = true
        atLaundryView.layer.cornerRadius = 6.0
        atLaundryView.clipsToBounds = true
        deliveredView.layer.cornerRadius = 6.0
        deliveredView.clipsToBounds = true
    }
    
    //MARK:- HeaderView Button Action
    func leftHeaderBtnAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
        self.performSegue(withIdentifier: "UserVC", sender: nil)
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
