//
//  UserVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 11/11/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class UserVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
    var items = ["Abbott","Abe","Addison","Aiken","Alaric","Alban","Algernon","Armstrong"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.configInitialView()
    }
    
    
    func configInitialView() {
        
        Helper().addHeader(forViewController: self,
                           isleftBtnImg: true, leftBtnName: "menu",
                           isleftBtnImg2: false, leftBtnName2: "",
                           centerName:"USER",
                           isRightBtnImg: true, rightBtnName: "back_arrow_white",
                           isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
    }
    
    //MARK:- HeaderView Button Action
    func leftHeaderBtnAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
        self.performSegue(withIdentifier: "OrderReviewVC", sender: nil)
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let nameTxt = cell.viewWithTag(1) as! UILabel
        nameTxt.text = String(format:"   %@",items[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
