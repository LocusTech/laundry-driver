//
//  LoginVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 07/10/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loginBtn.layer.cornerRadius = 4.0
        loginBtn.clipsToBounds = true
    }
    
    @IBAction func didTapOnLoginBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "HomeVC", sender: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "HomeVC" {
            
        }
    }
    
}

