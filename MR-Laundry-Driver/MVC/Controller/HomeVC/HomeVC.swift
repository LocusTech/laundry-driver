//
//  HomeVC.swift
//  MR-Laundry-Driver
//
//  Created by LocusTech on 07/10/17.
//  Copyright © 2017 LocusTech. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        self.configInitialView()
    }


    func configInitialView() {
        
        Helper().addHeader(forViewController: self, isleftBtnImg: true, leftBtnName: "menu", isleftBtnImg2: false, leftBtnName2: "", centerName:"MY-LAUNDRY", isRightBtnImg: false, rightBtnName: "", isRightBtnImg2: false, rightBtnName2: "", nibIndex: 0, isFooterSeprator: false)
    }
    
    //MARK:- HeaderView Button Action
    func leftHeaderBtnAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func leftHeaderBtnAction2() {
    }
    
    func rightHeaderBtnAction() {
    }
    
    func rightHeaderBtnAction2() {
        
    }
    
    //MARK:- HeaderView Button Action
    @IBAction func didTapOnPickUpBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectTimeVC", sender: nil)
    }

    @IBAction func didTapOnDelivery(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectTimeVC", sender: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}

