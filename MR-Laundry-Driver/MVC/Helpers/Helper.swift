//
//  Helper.swift
//  MR-Laundry
//
//  Created by LocusTech on 14/06/17.
//  Copyright © 2017 Locus. All rights reserved.
//
import UIKit
import CoreTelephony
import Foundation

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


protocol HelperActionSheetDelegate {
    func didSelectOnHelperActionSheet(_ selectedTitle:NSString)
}


open class Helper: NSObject {
    
    var gsmAlert = UIAlertController()
    var delegate: HelperActionSheetDelegate?
    static let appTittle:String = "Check Mate"
    
    class var sharedInstance: Helper {
        struct Static {
            static let instance = Helper()
        }
        return Static.instance
    }
    
    // MARK: - CheckMate
    func addHeaderPresent(forViewController:UIViewController,
                          isleftBtnImg:Bool,
                          leftBtnName:NSString,
                          isleftBtnImg2:Bool,
                          leftBtnName2:NSString,
                          centerName:NSString,
                          isRightBtnImg:Bool,
                          rightBtnName:NSString,
                          isRightBtnImg2:Bool,
                          rightBtnName2:NSString,
                          nibIndex: Int,
                          isFooterSeprator:Bool) {
        
        forViewController.navigationController?.navigationBar.isHidden = true
        let header:HeaderView =  Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![nibIndex] as! HeaderView
        
        header.setFrame(CGRect(x:0, y:0,width: Int(forViewController.view.frame.size.width),height: 64),
                        isFooterSeprator: isFooterSeprator,
                        isleftBtnImg: isleftBtnImg,
                        leftBtnName: leftBtnName,
                        isleft2BtnImg: isleftBtnImg2,
                        leftBtn2Name: leftBtnName2,
                        centerName: centerName,
                        isRightBtnImg: isRightBtnImg,
                        rightBtnName: rightBtnName,
                        isRight2BtnImg: isRightBtnImg2,
                        rightBtn2Name: rightBtnName2,
                        fromViewController: forViewController)
        
        header.tag = 1003
        forViewController.view .addSubview(header)
        
        //Button Actions
        //leftHeaderBtnAction
        //leftHeaderBtnAction2
        //rightHeaderBtnAction
        //rightHeaderBtnAction2
    }
    
    
    // MARK: - CheckMate
    func addHeader(forViewController:UIViewController,
                   isleftBtnImg:Bool,
                   leftBtnName:NSString,
                   isleftBtnImg2:Bool,
                   leftBtnName2:NSString,
                   centerName:NSString,
                   isRightBtnImg:Bool,
                   rightBtnName:NSString,
                   isRightBtnImg2:Bool,
                   rightBtnName2:NSString,
                   nibIndex: Int,
                   isFooterSeprator:Bool) {
        
        forViewController.navigationController?.navigationBar.isHidden = true
        let header:HeaderView =  Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![nibIndex] as! HeaderView
        
        header.setFrame(CGRect(x:0, y:0,width: Int(forViewController.view.frame.size.width),height: 64),
                        isFooterSeprator: isFooterSeprator,
                        isleftBtnImg: isleftBtnImg,
                        leftBtnName: leftBtnName,
                        isleft2BtnImg: isleftBtnImg2,
                        leftBtn2Name: leftBtnName2,
                        centerName: centerName,
                        isRightBtnImg: isRightBtnImg,
                        rightBtnName: rightBtnName,
                        isRight2BtnImg: isRightBtnImg2,
                        rightBtn2Name: rightBtnName2,
                        fromViewController: forViewController,
                        fromNavigation: forViewController.navigationController!)
        
        header.tag = 1003
        forViewController.view .addSubview(header)
        
        //Button Actions
        //leftHeaderBtnAction
        //leftHeaderBtnAction2
        //rightHeaderBtnAction
        //rightHeaderBtnAction2
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]  //JSONSerialization.ReadingOptions.mutableContainers
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    
    func showCameraPermissionDeniedAlert(controller: UIViewController) {
        let alert = UIAlertController(title: Helper.appTittle as String, message: "It looks like your  settings are preventing us to access your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select Privacy in the list.\n\n4. Select the Camera and Turn on.\n\n5. Open this app and try again.", preferredStyle: UIAlertControllerStyle.alert);
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url as URL)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showPhotoGalaryPermissionDeniedAlert(controller: UIViewController) {
        let alert = UIAlertController(title: Helper.appTittle as String, message: "It looks like your  settings are preventing us to access your Photoes. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select Privacy in the list.\n\n4. Select the Photoes and Turn on.\n\n5. Open this app and try again.", preferredStyle: UIAlertControllerStyle.alert);
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url as URL)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithRootViewController(message: String, controller: UIViewController) {
        
        let alert = UIAlertController(title: Helper.appTittle as String, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction)in
            _ = controller.navigationController?.popToRootViewController(animated: true)
        }));
        controller.present(alert, animated: true, completion: nil)
    }
    
    /*
     func gotoHome(callingController:UIViewController) {
     UIApplication.shared.statusBarStyle = .default
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
     appDelegate.window?.rootViewController = tabBarController
     }*/
    
    
    func numberOfLinesInLabel(yourString: String, labelWidth: CGFloat, labelHeight: CGFloat, font: UIFont) -> Int {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = labelHeight
        paragraphStyle.maximumLineHeight = labelHeight
        paragraphStyle.lineBreakMode = .byCharWrapping
        
        let attributes: [String: AnyObject] = [NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle]
        
        let constrain = CGSize(width: labelWidth, height: CGFloat(Float.infinity))
        
        let size = yourString.size(attributes: attributes)
        let stringWidth = size.width
        
        let numberOfLines = ceil(Double(stringWidth/constrain.width))
        
        return Int(numberOfLines)
    }
    
    // MARK:- image Processing
    // convert images into base64 and keep them into string
    func convertImageToBase64(image: UIImage) -> String {
        
        // let userImage:UIImage = UIImage(named: "Your-Image_name")!
        //   let imageData = UIImageJPEGRepresentation(image, 0.8)
        // let image = imageData as NSData
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
        return base64String
    }// end convertImageToBase64
    
    
    // prgm mark ----
    // convert images into base64 and keep them into string
    func decodeBase64(toImage strEncodeData: String) -> UIImage {
        
        let dataDecoded  = NSData(base64Encoded: strEncodeData, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        let image = UIImage(data: dataDecoded as Data)
        return image!
    }
    
    func showNoData(parnetView: UIView, noDataMessage: String, headerHeight: CGFloat) -> UIView {
        let noDataView = UIView()
        noDataView.backgroundColor = .red
        noDataView.frame = CGRect.init(x: 0, y: headerHeight, width: parnetView.frame.size.width, height: (parnetView.frame.size.height - headerHeight))
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect.init(x: 0, y: 0, width: parnetView.frame.size.width, height: (parnetView.frame.size.height - headerHeight))
        noDataLabel.text = noDataMessage
        noDataLabel.backgroundColor = .white
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.setAppFontBold(size: 16.0)
        noDataView.addSubview(noDataLabel)
        return noDataView
    }
    
    func showAlertPopUPWithMessage(_ msg:NSString,bgColor:UIColor?, controller:UIViewController) {
        let crmAlert:AlertPopUpView =  Bundle.main.loadNibNamed("AlertPopUpView", owner: nil, options: nil)![0] as! AlertPopUpView
        crmAlert.showAlertPopUp(msg as String, bgColor: bgColor, forViewController: controller)
        crmAlert.tag = 1002
        UIApplication.shared.keyWindow?.addSubview(crmAlert);
    }
    
    func saveSelectedLanguage(lang: String) ->Void{
        
        print(lang)
        UserDefaults.standard.set(lang, forKey: "language")
        
    }
    func getSelectedLanguage() -> String {
        let value = UserDefaults.standard.object(forKey: "language")
        
        if value != nil {
            return value as! String
        }
        return ""
    }
    
    

}

import UIKit
import Foundation
public extension UIFont {
    
     class func setAppFontRegular( size:CGFloat)-> (UIFont) {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    
     class func setAppFontMedium( size:CGFloat)-> (UIFont) {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
    
     class func setAppFontLight( size:CGFloat)-> (UIFont) {
        return UIFont(name: "Roboto-Light", size: size)!
    }
    
    class func setAppFontBold( size:CGFloat)->(UIFont) {
        return UIFont(name: "Roboto-Bold", size: size)!
    }
    
    class func setAppFontItalic( size:CGFloat)->(UIFont) {
        return UIFont(name: "Roboto-Italic", size: size)!
    }
}

// MARK: - String
extension Helper {
    func isNilOrEmpty(_ string: NSString?) -> Bool {
        switch string {
        case .some(let nonNilString):
            return nonNilString.length == 0
        default:
            return true
        }
    }
    
    func noNilString(_ string: NSString?) -> NSString {
        if (string?.length > 0  && (string != ("null"))) {
            return string!
        }
        else{
            return ""
        }
    }
    func isValidMobile(value: String) -> Bool {
        
        let regEx = "^[0-9]{10}$"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isValidPinCode (value: String)-> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[0-9]{6}$", options: [.caseInsensitive])
        return regex.firstMatch(in: value, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, value.characters.count)) != nil
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
}

extension String {
    
    func isValidEmailAddress ()-> Bool {
        let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: [.caseInsensitive])
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, characters.count)) != nil
    }
    
    //Alert
    func isValidEmail(value: String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isValidMobile(value: String) -> Bool {
        
        let regEx = "^[0-9]{10}$"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isValidPinCode ()-> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[0-9]{6}$", options: [.caseInsensitive])
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, characters.count)) != nil
    }
    
    func isStringValueAvailable()-> String {
        return self
    }
    
}

//MARK:- Dictionary Validation
extension NSDictionary {
    
    func isHaveBoolValue(_ key:NSString) -> (Bool) {
        if let value = self[key] as? Bool {
            return value
        }
        return false
    }
    
    func isHaveStringValue(_ key:NSString) -> (String) {
        if let value = self[key] as? NSString {
            if (value.length > 0  && (value != ("null"))) {
                return value as (String)
            }
            else{
                return ""
            }
        }
        return ""
    }
    
    func isHaveArrayValue(_ key:NSString) -> (NSArray) {
        if let value = self[key] as? NSArray {
            if(value.count > 0) {
                return value
            }
            else {
                return []
            }
        }
        return []
    }
    
    func isHaveMutableArrayValue(_ key:NSString) -> (NSMutableArray) {
        if let value = self[key] as? NSMutableArray {
            if(value.count > 0) {
                return value
            }
            else {
                return []
            }
        }
        return []
    }
    
    func isHaveDictionayValude(_ key:NSString) -> (NSDictionary) {
        if let value = self[key] as? NSDictionary {
            
            return value
            
        }
        return [String:AnyObject]() as (NSDictionary)
    }
    
    func isHaveMutableDictionayValude(_ key:NSString) -> (NSMutableDictionary) {
        if let value = self[key] as? NSMutableDictionary {
            
            return value
            
        }
        return ([String:AnyObject]() as? (NSMutableDictionary))!
    }
    
    func isHaveIntValue(_ key:NSString) -> Int {
        if let value = self[key] as? Int {
            return value
        }
        return 0
    }
    
}

//MARK: - UIImageView
extension UIImageView {
    public func imageFromUrl(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            
            NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler: { (response, data, error) in
                if let imageData = data as Data? {
                    self.image = UIImage(data: imageData)
                }
            })
        }
    }
}

import AVFoundation
func checkCameraPermisson() -> Void{
    
    if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
        // Already Authorized
    }
    else {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
            if granted == true {
                // User granted
            }
            else {
                //                // User Rejected
            }
        });
    }
}


